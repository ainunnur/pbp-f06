from django.contrib import admin
from .models import Article, Comment, Profile, Question

admin.site.register(Article)
admin.site.register(Comment)
admin.site.register(Profile)
admin.site.register(Question)