function check_pass(){
    let p = $("#pas").val();
    let cp = $("#cpas").val();
    
    if(p==cp){
        $("#pas").css("border","1px solid green");
        $("#cpas").css("border","1px solid green");
        
        $("#sbbtn").removeAttr("disabled");
    }else{
        $("#pas").css("border","1px solid red");
        $("#cpas").css("border","1px solid red");

        $("#sbbtn").attr("disabled","disabled");
    }
}