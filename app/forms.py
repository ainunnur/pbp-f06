from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from .models import Profile, Question
# Create your forms here.


class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user

class CommentForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea)
    text.widget.attrs.update({
        'class': 'ml-3 rounded',
        'id':'my-textarea',
        'placeholder':'Write your comment here...',
    })

class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['first_name', 'last_name', 'gender',
                  'number_phone', 'bio', 'province', 'profile_pic']


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ("username", "question")
