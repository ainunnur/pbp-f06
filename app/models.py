from django.db import models
from django.contrib.auth.models import User


class Article(models.Model):
    #Note: Well, karena fiturnya (yg di readme repo) user cm bisa liat artikel, gw asumsi user ga bisa bikin artikel.
    #Kalo mau bikin artikel, bikin di admin aja.
    author = models.ForeignKey(User, verbose_name ="author", on_delete=models.SET_NULL, related_name="articles", null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    title = models.CharField(max_length=50)
    text = models.TextField(verbose_name="text")
    liked = models.ManyToManyField(User, verbose_name="liked", related_name="liked", null=True, blank=True)
    viewed= models.PositiveBigIntegerField(editable=False, blank=True, default=0)
    class Meta:
        ordering = ['created', "title"]
class Comment(models.Model):
    author = models.ForeignKey(User, verbose_name ="author", on_delete=models.SET_NULL, related_name="comments", null=True)
    created = models.DateTimeField(auto_now=True, editable=False, blank=True)
    text = models.TextField(verbose_name="content")
    article = models.ForeignKey(Article, verbose_name="article", on_delete=models.CASCADE, null=True, related_name="comments")

    class Meta:
        ordering = ['created']
class Profile(models.Model):
    user = models.OneToOneField(
        User, null=True, blank=True, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)
    gender = models.CharField(max_length=200, null=True)
    number_phone = models.CharField(max_length=200, null=True)
    bio = models.CharField(max_length=200, null=True)
    province = models.CharField(max_length=200, null=True)
    date_of_birth = models.DateField(null=True)
    profile_pic = models.ImageField(
        default="undraw_profile.svg", upload_to='users/', null=True, blank=True)

    def __str__(self):
        return f'{self.user.username}-Profile'

class Question(models.Model):
    username = models.CharField(max_length=100)
    question = models.TextField()
    date = models.DateTimeField(auto_now_add=True, blank=True)
