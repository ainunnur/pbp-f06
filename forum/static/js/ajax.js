const getReplies = (e) => {
    e.innerHTML = (e.innerHTML == "Lihat Balasan") ? "Kembali" : "Lihat Balasan";
    let id = e.getAttribute("value");
    let target = document.getElementById("replies" + id);
    if (target.innerHTML != "") {
        return;
    }
    $.ajax({
        url: `/replies/${id}`,
        method: 'GET',
        dataType: 'json',
        success: function(response) {
            target.innerHTML = `<hr><h3 class="mr-3 ml-3">Balasan</h3>`;
            if (response.length === 0) {
                let element = document.createElement('p');
                element.innerHTML = "Belum ada balasan untuk sementara ini."
                element.setAttribute("class", "mr-3 ml-3 mb-3");
                target.appendChild(element);
                return;
            }
            response.forEach((obj) => {
                let val = obj.fields;
                let element = document.createElement('div');
                element.setAttribute("class", "card card-body mr-3 ml-3 mb-3");
                element.innerHTML = `
                    Dari: ${val.From}
                    <br>
                    ${val.reply}
                `
                target.appendChild(element);
            });
        }
    });
}